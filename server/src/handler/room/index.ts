import {Socket} from "socket.io";
import {v4 as uuidV4} from "uuid";


let rooms : Record<string,string[]> = {};

interface IRoomParams{
    roomId : string;
    peerId : string;
}

export function roomHandler(socket:Socket){
    const createRoom = ()=>{
        const roomId = uuidV4();
        rooms[`${roomId}`] = [];
        console.log("room created ")
        socket.emit("room-created",{
            roomId
        })
    }

    const joinRoom = ({roomId, peerId} : IRoomParams)=>{
        console.log("User joined room ",roomId, peerId);
        console.log("The room ",rooms[roomId]);
        if(rooms[roomId]){
            rooms[roomId].push(peerId);
            socket.join(roomId);
            socket.to(roomId).emit("users-joined",{peerId})
            socket.emit("get-users",{roomId,participants:rooms[roomId]})
    
            socket.on("disconnect",()=>{
                console.log("The user left  the room ",peerId);
                leaveRoom({roomId,peerId})
            })
        }
       
    }

    function leaveRoom({roomId,peerId}:IRoomParams){
       rooms[roomId] = rooms[roomId].filter((id)=>id!==peerId);
       socket.to(roomId).emit("user-disconnected",peerId)
    }

    socket.on("join-room",joinRoom);
    
    socket.on("create-room",createRoom)
}